PRTG Template for the Fujitsu ETERNUS SAN.
===========================================

Download Instructions
=========================
 [A zip file containing all the files in the project can be downloaded from the
repository](https://gitlab.com/PRTG/Device-Templates/fujitsu-eternus/-/jobs/artifacts/master/download?job=PRTGDistZip)


Template Notes
=========================
The MIB for the Eternus SAN is fairly large and has a lot of small tables
that don't lend themselves too well to PRTG's Table templates.
Like the Fujitsu PRIMERGY line, the tables of drives / volumens/ and sensors,
are all "fully populated" with entries that are "invalid" or "unavalable".
This creates a problem for the Template instantiation engine, since there
is currently not a way to ignore those rows.
So, unfortunately, they will have to be deleted manually...


Installation Instructions
=========================
[Generic PRTG custom sensor instructions INSTALL_PRTG.md](./INSTALL_PRTG.md)

To install the PRTG Backup sensor, unzip the installer package into the PRTG Executable directory,
as outlined in Install_PRTG.md

To install and configure the sensor, pick the device (Usually the Probe device) and add
an "EXE/Script Advanced" and select "PRTGBackup.ps1" as the "EXE/Script".



Troubleshooting
================
The Backup script is written in Powershell
In case there are issues with the script please follow the steps outlined in the [PRTG Powershell trouble shooting guideline](
https://kb.paessler.com/en/topic/71356-guide-for-powershell-based-custom-sensors)
