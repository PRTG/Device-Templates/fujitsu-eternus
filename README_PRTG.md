## PRTG Device Template for Checkpoint Next Generation Firewall devices

This project is a custom device template that cna be used to monitor a Checkpoint Next Generation Firewall in PRTG using the auto-discovery for sensor creation.

### Changes
 2018-08-27 Added support for CheckPoint 60K series sensors. 
   See at bottom of this file



### Download
Please use this link to download [Checkpoint Install package](https://gitlab.com/PRTG/Device-Templates/Checkpoint/-/jobs/artifacts/master/download?job=PRTGDistZip)


## Installation Instructions
Please refer to INSTALL.md or refer to the following KB-Post:
- [Monitoring Check Point Firewalls with PRTG?](https://kb.paessler.com/en/topic/76204)


[Creating PRTG Device Templates](https://www.paessler.com/manuals/prtg/create_device_template)

[Creating PRTG Custom Scripts](https://kb.paessler.com/en/topic/71356-guide-for-powershell-based-custom-sensors)

The template project has a standard directory structure:
All the files in the PRTG subdirectory needs to go into the PRTG program directory
[How and where does PRTG store its data?](https://kb.paessler.com/en/topic/463-how-and-where-does-prtg-store-its-data)
The other files are for documentation and testing.
<div class="panel panel-info">
Directory Layout:

<div class="panel-body">
<pre>
 ProjectName
   + - traces		     (files to use for testing, SNMP traces etc.)
   + - PRTG                  (hierarchy that goes into the PRTG directory)
        + - Custom Sensors   (Where PRTG stores Custom senosrs)
        + - devicetemplates  (Where PRTG stores the device templates)
        + - lookups          (Where PRTG stores the lookups)
            + - custom       (Where PRTG stores custom lookups)
        + - MIB              (Where PRTG stores MIBs)
        + - notifications    (Where PRTG stores Custom Notifications)
        + - snmplibs         (Where PRTG stores imported custom OID Libraries)
        + - webroot          (PRTG webgui)
            + - icons        (PRTG webgui icons)
                + - devices  (PRTG device icons)
</pre>
</div>
</div>

### Included Files

#### Custom Sensors
PRTG Custom Sensor files.

#### devicetemplates (xxx.odt) 
PRTG device template files.

#### lookups\custom (xxx.ovl) 
PRTG lookup files for use with sensors with lookup values

#### notifications
Custom PRTG Notifications.

#### MIB
Manufacturer supplied MIB files to include. These will be loaded into PRTG Core server on startup.

#### snmplibs (xxx.oidlib)
Files imported from MIB files by the [Paessler MIB importer](https://www.paessler.com/tools/mibimporter).

#### webroot\icons\devices (vendors_xxx.png/svg)
Icon files used by PRTG to display in the tree. 
<pre>
 -PNG's: Should be 14.14 pixels.
 -SVG's; Should be small images for best presentation 
</pre>


### Known Issues
 * Hardware sensors only show the Units in label
   Currently Not possible to extract unit for channel using OID in SNMP Custom Advanced / Table sensors.
 * ASG Counters Show Invalid Integer for table entries with "N/A"
  Currently not possible to filter out invalid values in template.
  As a work-arround, pause the invalid ones (that way they don't show up again). 

 * ASG SENSOR Status lookup unclear. what the lookup values mean
    Status 1 -> In Range, 0 - either Not available or error
    Waiting for clarification from manufacturer


### Changes
 2018-08-27 Added support for CheckPoint 60K series sensors

 ![60K Series Hardware Sensor Sensor](./Images/Checkpoint64K_Sensor.png)
60K Series Hardware Sensor (Table)

 ![60K ASG Counters Sensor](./Images/Checkpoint64K_ASGCounters.png)
60K ASG Counters Sensor

 ![60K IPv6 Performance Sensor](./Images/Checkpoint64K_ipv6Perf.png)
60K IPv6 Performance Sensor

 ![60K IPv6 Performance Sensor](./Images/Checkpoint64K_ipv6Perf.png)
60K IPv6 Performance Sensor
